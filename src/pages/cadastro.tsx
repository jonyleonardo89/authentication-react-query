import Button from '@mui/material/Button';
import { useAdicionarPost, useDeletarPost, useAtualizarPost } from '../hooks/queryHooks';

const values = {
    "title": "Novo post",
    "body": "Novo corpo",
}

const valuesUpdate = {
    "title": "POst 7",
    "body": "Novo corpo 7",
}

function cadastro() {
    const {usuarioAdicionar}  = useAdicionarPost();
    const { usuarioDeletar } = useDeletarPost(6);
    const { usuarioAtualizar } = useAtualizarPost(7)

    async function handleSubmit(e) {
        e.preventDefault();
        await usuarioAdicionar(values, {
            onSuccess: () => {
                alert("Cadastrado com sucesso")
            }
        })  
    }
    async function handleUpdate() {
        await usuarioAtualizar(valuesUpdate, {
            onSucces: () => {
                    alert("Atualizado com Sucesso")
                }
        })
    }

    async function handleDelete(e) {
        await usuarioDeletar(values,{
            onSuccess: () => {
                alert('excluido com sucesso')
            },
        });
    }
    return ( 
        <>
            <Button variant="contained" color="success" onClick={handleSubmit}>
                fazer Post
            </Button>

            <Button variant="contained" color="secondary" style={{ marginLeft: '10px'}} onClick={handleUpdate}>
                Atualizar
            </Button>

            <Button variant="contained" color="inherit" style={{background: '#F26', color: '#FFF', marginLeft: '10px'}} onClick={handleDelete}>
                Deletar
            </Button>
            
        </>

    );
}

export default cadastro;