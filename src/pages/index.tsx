import type { NextPage } from 'next'
import Lista from '../components/Lista';
import { useQuery } from 'react-query'
import axios from 'axios';
 import { useNomes } from '../hooks/queryHooks'

const Home: NextPage = () => {

    const { todos } = useNomes(); 
    console.log(todos)

    return (
        <>
            {
            todos?.data?.map((result) => (
                <Lista key={result.id}>
                    {result.first_name}
                </Lista>
            ))}
        
        </>
    )
}

export default Home
