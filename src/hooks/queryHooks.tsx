import { useQuery, useQueryClient } from 'react-query';
import { useMutation } from 'react-query';
import axios from 'axios';

const newPost = {
    title: 'A new post',
    body: 'This is the body of the new post'
};

export function useNomes() {
    const { data } = useQuery('Nomes',
        async () => {
            const result = await axios({
                method: 'GET',
                url: `https://reqres.in/api/users`,
                params: {},
            });
            return result.data;
        })
    return {todos: data}
}

export function useAdicionarPost() {
    const queryClient = useQueryClient();
    const { mutateAsync } = useMutation(
        (values) =>
        axios({
            method: 'POST',
            url: `http://localhost:3002/posts`,
            data: {
                values,
            },
        }),
        {
            //INVALIDA QUERY DE CHAVE "nomes"  E FAZ A BUSCA DOS DADOS EM CACHE NOVAMENTE
            onSuccess() {
                queryClient.invalidateQueries('Nomes');
        },
    });
    return { usuarioAdicionar: mutateAsync };
}

export function useAtualizarPost(postId: number) {
    const queryClient = useQueryClient();
    const { mutateAsync } = useMutation(
        (values) =>
            axios({
                method: 'PUT',
                url: `http://localhost:3002/posts/${postId}`,
                data: {
                    ...values,
                }
            }),
        {
            onSuccess: () => {
                queryClient.invalidateQueries('Nomes');
            }
        }
    );
    return { usuarioAtualizar: mutateAsync }
}

export function useDeletarPost(postId: number) {
    const queryClient = useQueryClient();
    const { mutateAsync } = useMutation(
        () =>
            axios({
                method: 'DELETE',
                url: `http://localhost:3002/posts/${postId}`,
            }),
        {
            onSuccess() {
                queryClient.invalidateQueries('Nomes');
            },
        }
    );
    return { usuarioDeletar: mutateAsync };
}
    

